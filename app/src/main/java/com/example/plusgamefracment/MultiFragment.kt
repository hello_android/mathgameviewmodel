package com.example.plusgamefracment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.plusgamefracment.databinding.FragmentMultiBinding
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MultiFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MultiFragment : Fragment() {
    private lateinit var binding: FragmentMultiBinding
    private lateinit var multiGameViewModel: MultiGameViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        multiGameViewModel = ViewModelProvider(this).get(MultiGameViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_multi, container, false)
        binding.multiGameViewModel  = multiGameViewModel

        multiGameViewModel.eventNextQuestion.observe(viewLifecycleOwner, Observer {hasEvent ->
            if(hasEvent) {
                enableAllButton()
                binding.invalidateAll()
                multiGameViewModel.onNextFinish()
            }

        })

        multiGameViewModel.eventHome.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                view?.findNavController()?.navigate(R.id.action_multiFragment_to_startGameMenuFragment)
                multiGameViewModel.changeToHomeFinish()

            }
        })

        multiGameViewModel.eventSelectChoice1.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                multiGameViewModel.onSelectChoice1Finish()
            }

        })

        multiGameViewModel.eventSelectChoice2.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                multiGameViewModel.onSelectChoice2Finish()
            }

        })

        multiGameViewModel.eventSelectChoice3.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                multiGameViewModel.onSelectChoice3Finish()
            }

        })


        return binding.root
    }
    fun disabledAllButton(){
        binding.apply {
            btn1.isEnabled =false
            btn2.isEnabled =false
            btn3.isEnabled =false
        }
    }
    fun enableAllButton(){
        binding.apply {
            btn1.isEnabled =true
            btn2.isEnabled =true
            btn3.isEnabled =true
        }
    }

}