package com.example.plusgamefracment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.plusgamefracment.databinding.FragmentStartGameMenuBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [StartGameMenuFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class StartGameMenuFragment : Fragment() {
    lateinit var binding: FragmentStartGameMenuBinding
    private lateinit var startGamesMenuViewModel: StartGameMenuViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        startGamesMenuViewModel = ViewModelProvider(this).get(StartGameMenuViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_start_game_menu, container, false)
        binding.startGamesMenuViewModel = startGamesMenuViewModel

//        startGamesMenuViewModel.score.observe(viewLifecycleOwner, Observer { newScore ->
//            binding.amountWrong.text = newScore.toString()
//
//        })
        startGamesMenuViewModel.eventPlus.observe(viewLifecycleOwner, Observer { hasNext ->

            if(hasNext){
                view?.findNavController()?.navigate(R.id.action_startGameMenuFragment_to_plusFragment)
                startGamesMenuViewModel.changeToPlusFinish()

            }


        })
        startGamesMenuViewModel.eventMinus.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                view?.findNavController()?.navigate(R.id.action_startGameMenuFragment_to_minusFragment)
                startGamesMenuViewModel.changeToMinusFinish()
            }

        })

        startGamesMenuViewModel.eventMulti.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                view?.findNavController()?.navigate(R.id.action_startGameMenuFragment_to_multiFragment)
                startGamesMenuViewModel.changeToMultiFinish()
            }

        })
        return binding.root

    }

}