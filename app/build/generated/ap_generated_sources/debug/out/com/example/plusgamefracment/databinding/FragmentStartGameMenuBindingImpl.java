package com.example.plusgamefracment.databinding;
import com.example.plusgamefracment.R;
import com.example.plusgamefracment.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentStartGameMenuBindingImpl extends FragmentStartGameMenuBinding implements com.example.plusgamefracment.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback3;
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    @Nullable
    private final android.view.View.OnClickListener mCallback2;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentStartGameMenuBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private FragmentStartGameMenuBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.Button) bindings[2]
            , (android.widget.Button) bindings[1]
            , (android.widget.Button) bindings[3]
            );
        this.btnMinusGame.setTag(null);
        this.btnMultiGame.setTag(null);
        this.btnSumGame.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        mCallback3 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 3);
        mCallback1 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 1);
        mCallback2 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.startGamesMenuViewModel == variableId) {
            setStartGamesMenuViewModel((com.example.plusgamefracment.StartGameMenuViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setStartGamesMenuViewModel(@Nullable com.example.plusgamefracment.StartGameMenuViewModel StartGamesMenuViewModel) {
        this.mStartGamesMenuViewModel = StartGamesMenuViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.startGamesMenuViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.example.plusgamefracment.StartGameMenuViewModel startGamesMenuViewModel = mStartGamesMenuViewModel;
        // batch finished
        if ((dirtyFlags & 0x2L) != 0) {
            // api target 1

            this.btnMinusGame.setOnClickListener(mCallback2);
            this.btnMultiGame.setOnClickListener(mCallback1);
            this.btnSumGame.setOnClickListener(mCallback3);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 3: {
                // localize variables for thread safety
                // startGamesMenuViewModel != null
                boolean startGamesMenuViewModelJavaLangObjectNull = false;
                // startGamesMenuViewModel
                com.example.plusgamefracment.StartGameMenuViewModel startGamesMenuViewModel = mStartGamesMenuViewModel;



                startGamesMenuViewModelJavaLangObjectNull = (startGamesMenuViewModel) != (null);
                if (startGamesMenuViewModelJavaLangObjectNull) {


                    startGamesMenuViewModel.changeToPlus();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // startGamesMenuViewModel != null
                boolean startGamesMenuViewModelJavaLangObjectNull = false;
                // startGamesMenuViewModel
                com.example.plusgamefracment.StartGameMenuViewModel startGamesMenuViewModel = mStartGamesMenuViewModel;



                startGamesMenuViewModelJavaLangObjectNull = (startGamesMenuViewModel) != (null);
                if (startGamesMenuViewModelJavaLangObjectNull) {


                    startGamesMenuViewModel.changeToMulti();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // startGamesMenuViewModel != null
                boolean startGamesMenuViewModelJavaLangObjectNull = false;
                // startGamesMenuViewModel
                com.example.plusgamefracment.StartGameMenuViewModel startGamesMenuViewModel = mStartGamesMenuViewModel;



                startGamesMenuViewModelJavaLangObjectNull = (startGamesMenuViewModel) != (null);
                if (startGamesMenuViewModelJavaLangObjectNull) {


                    startGamesMenuViewModel.changeToMinus();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): startGamesMenuViewModel
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}