package com.example.plusgamefracment.databinding;
import com.example.plusgamefracment.R;
import com.example.plusgamefracment.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMultiBindingImpl extends FragmentMultiBinding implements com.example.plusgamefracment.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.txtTitle, 11);
        sViewsWithIds.put(R.id.textView4, 12);
        sViewsWithIds.put(R.id.txtCorrect, 13);
        sViewsWithIds.put(R.id.txtWrong, 14);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback8;
    @Nullable
    private final android.view.View.OnClickListener mCallback6;
    @Nullable
    private final android.view.View.OnClickListener mCallback4;
    @Nullable
    private final android.view.View.OnClickListener mCallback7;
    @Nullable
    private final android.view.View.OnClickListener mCallback5;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentMultiBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private FragmentMultiBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 7
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[9]
            , (android.widget.Button) bindings[5]
            , (android.widget.Button) bindings[6]
            , (android.widget.Button) bindings[7]
            , (android.widget.Button) bindings[1]
            , (android.widget.Button) bindings[10]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[14]
            );
        this.amountCorrect.setTag(null);
        this.amountWrong.setTag(null);
        this.btn1.setTag(null);
        this.btn2.setTag(null);
        this.btn3.setTag(null);
        this.btnHome.setTag(null);
        this.btnNext.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.textView1.setTag(null);
        this.textView2.setTag(null);
        this.txtAns.setTag(null);
        setRootTag(root);
        // listeners
        mCallback8 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 5);
        mCallback6 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 3);
        mCallback4 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 1);
        mCallback7 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 4);
        mCallback5 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x100L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.multiGameViewModel == variableId) {
            setMultiGameViewModel((com.example.plusgamefracment.MultiGameViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setMultiGameViewModel(@Nullable com.example.plusgamefracment.MultiGameViewModel MultiGameViewModel) {
        this.mMultiGameViewModel = MultiGameViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x80L;
        }
        notifyPropertyChanged(BR.multiGameViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeMultiGameViewModelChoice3((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 1 :
                return onChangeMultiGameViewModelChoice1((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 2 :
                return onChangeMultiGameViewModelChoice2((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 3 :
                return onChangeMultiGameViewModelNum2((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 4 :
                return onChangeMultiGameViewModelNum1((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 5 :
                return onChangeMultiGameViewModelResultText((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
            case 6 :
                return onChangeMultiGameViewModelScore((androidx.lifecycle.LiveData<com.example.plusgamefracment.Score>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeMultiGameViewModelChoice3(androidx.lifecycle.LiveData<java.lang.Integer> MultiGameViewModelChoice3, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMultiGameViewModelChoice1(androidx.lifecycle.LiveData<java.lang.Integer> MultiGameViewModelChoice1, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMultiGameViewModelChoice2(androidx.lifecycle.LiveData<java.lang.Integer> MultiGameViewModelChoice2, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMultiGameViewModelNum2(androidx.lifecycle.LiveData<java.lang.Integer> MultiGameViewModelNum2, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMultiGameViewModelNum1(androidx.lifecycle.LiveData<java.lang.Integer> MultiGameViewModelNum1, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMultiGameViewModelResultText(androidx.lifecycle.LiveData<java.lang.String> MultiGameViewModelResultText, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMultiGameViewModelScore(androidx.lifecycle.LiveData<com.example.plusgamefracment.Score> MultiGameViewModelScore, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Integer multiGameViewModelChoice1GetValue = null;
        java.lang.String multiGameViewModelResultTextGetValue = null;
        java.lang.String javaLangStringMultiGameViewModelChoice3 = null;
        androidx.lifecycle.LiveData<java.lang.Integer> multiGameViewModelChoice3 = null;
        java.lang.String javaLangStringMultiGameViewModelScoreCorrect = null;
        java.lang.String javaLangStringMultiGameViewModelNum1 = null;
        androidx.lifecycle.LiveData<java.lang.Integer> multiGameViewModelChoice1 = null;
        androidx.lifecycle.LiveData<java.lang.Integer> multiGameViewModelChoice2 = null;
        androidx.lifecycle.LiveData<java.lang.Integer> multiGameViewModelNum2 = null;
        androidx.lifecycle.LiveData<java.lang.Integer> multiGameViewModelNum1 = null;
        int multiGameViewModelScoreCorrect = 0;
        java.lang.Integer multiGameViewModelChoice3GetValue = null;
        int multiGameViewModelScoreWrong = 0;
        java.lang.String javaLangStringMultiGameViewModelChoice1 = null;
        androidx.lifecycle.LiveData<java.lang.String> multiGameViewModelResultText = null;
        java.lang.Integer multiGameViewModelChoice2GetValue = null;
        com.example.plusgamefracment.Score multiGameViewModelScoreGetValue = null;
        java.lang.Integer multiGameViewModelNum2GetValue = null;
        com.example.plusgamefracment.MultiGameViewModel multiGameViewModel = mMultiGameViewModel;
        java.lang.String javaLangStringMultiGameViewModelChoice2 = null;
        java.lang.Integer multiGameViewModelNum1GetValue = null;
        androidx.lifecycle.LiveData<com.example.plusgamefracment.Score> multiGameViewModelScore = null;
        java.lang.String javaLangStringMultiGameViewModelNum2 = null;
        java.lang.String javaLangStringMultiGameViewModelScoreWrong = null;

        if ((dirtyFlags & 0x1ffL) != 0) {


            if ((dirtyFlags & 0x181L) != 0) {

                    if (multiGameViewModel != null) {
                        // read multiGameViewModel.choice3
                        multiGameViewModelChoice3 = multiGameViewModel.getChoice3();
                    }
                    updateLiveDataRegistration(0, multiGameViewModelChoice3);


                    if (multiGameViewModelChoice3 != null) {
                        // read multiGameViewModel.choice3.getValue()
                        multiGameViewModelChoice3GetValue = multiGameViewModelChoice3.getValue();
                    }


                    // read ("") + (multiGameViewModel.choice3.getValue())
                    javaLangStringMultiGameViewModelChoice3 = ("") + (multiGameViewModelChoice3GetValue);
            }
            if ((dirtyFlags & 0x182L) != 0) {

                    if (multiGameViewModel != null) {
                        // read multiGameViewModel.choice1
                        multiGameViewModelChoice1 = multiGameViewModel.getChoice1();
                    }
                    updateLiveDataRegistration(1, multiGameViewModelChoice1);


                    if (multiGameViewModelChoice1 != null) {
                        // read multiGameViewModel.choice1.getValue()
                        multiGameViewModelChoice1GetValue = multiGameViewModelChoice1.getValue();
                    }


                    // read ("") + (multiGameViewModel.choice1.getValue())
                    javaLangStringMultiGameViewModelChoice1 = ("") + (multiGameViewModelChoice1GetValue);
            }
            if ((dirtyFlags & 0x184L) != 0) {

                    if (multiGameViewModel != null) {
                        // read multiGameViewModel.choice2
                        multiGameViewModelChoice2 = multiGameViewModel.getChoice2();
                    }
                    updateLiveDataRegistration(2, multiGameViewModelChoice2);


                    if (multiGameViewModelChoice2 != null) {
                        // read multiGameViewModel.choice2.getValue()
                        multiGameViewModelChoice2GetValue = multiGameViewModelChoice2.getValue();
                    }


                    // read ("") + (multiGameViewModel.choice2.getValue())
                    javaLangStringMultiGameViewModelChoice2 = ("") + (multiGameViewModelChoice2GetValue);
            }
            if ((dirtyFlags & 0x188L) != 0) {

                    if (multiGameViewModel != null) {
                        // read multiGameViewModel.num2
                        multiGameViewModelNum2 = multiGameViewModel.getNum2();
                    }
                    updateLiveDataRegistration(3, multiGameViewModelNum2);


                    if (multiGameViewModelNum2 != null) {
                        // read multiGameViewModel.num2.getValue()
                        multiGameViewModelNum2GetValue = multiGameViewModelNum2.getValue();
                    }


                    // read ("") + (multiGameViewModel.num2.getValue())
                    javaLangStringMultiGameViewModelNum2 = ("") + (multiGameViewModelNum2GetValue);
            }
            if ((dirtyFlags & 0x190L) != 0) {

                    if (multiGameViewModel != null) {
                        // read multiGameViewModel.num1
                        multiGameViewModelNum1 = multiGameViewModel.getNum1();
                    }
                    updateLiveDataRegistration(4, multiGameViewModelNum1);


                    if (multiGameViewModelNum1 != null) {
                        // read multiGameViewModel.num1.getValue()
                        multiGameViewModelNum1GetValue = multiGameViewModelNum1.getValue();
                    }


                    // read ("") + (multiGameViewModel.num1.getValue())
                    javaLangStringMultiGameViewModelNum1 = ("") + (multiGameViewModelNum1GetValue);
            }
            if ((dirtyFlags & 0x1a0L) != 0) {

                    if (multiGameViewModel != null) {
                        // read multiGameViewModel.resultText
                        multiGameViewModelResultText = multiGameViewModel.getResultText();
                    }
                    updateLiveDataRegistration(5, multiGameViewModelResultText);


                    if (multiGameViewModelResultText != null) {
                        // read multiGameViewModel.resultText.getValue()
                        multiGameViewModelResultTextGetValue = multiGameViewModelResultText.getValue();
                    }
            }
            if ((dirtyFlags & 0x1c0L) != 0) {

                    if (multiGameViewModel != null) {
                        // read multiGameViewModel.score
                        multiGameViewModelScore = multiGameViewModel.getScore();
                    }
                    updateLiveDataRegistration(6, multiGameViewModelScore);


                    if (multiGameViewModelScore != null) {
                        // read multiGameViewModel.score.getValue()
                        multiGameViewModelScoreGetValue = multiGameViewModelScore.getValue();
                    }


                    if (multiGameViewModelScoreGetValue != null) {
                        // read multiGameViewModel.score.getValue().correct
                        multiGameViewModelScoreCorrect = multiGameViewModelScoreGetValue.getCorrect();
                        // read multiGameViewModel.score.getValue().wrong
                        multiGameViewModelScoreWrong = multiGameViewModelScoreGetValue.getWrong();
                    }


                    // read ("") + (multiGameViewModel.score.getValue().correct)
                    javaLangStringMultiGameViewModelScoreCorrect = ("") + (multiGameViewModelScoreCorrect);
                    // read ("") + (multiGameViewModel.score.getValue().wrong)
                    javaLangStringMultiGameViewModelScoreWrong = ("") + (multiGameViewModelScoreWrong);
            }
        }
        // batch finished
        if ((dirtyFlags & 0x1c0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.amountCorrect, javaLangStringMultiGameViewModelScoreCorrect);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.amountWrong, javaLangStringMultiGameViewModelScoreWrong);
        }
        if ((dirtyFlags & 0x182L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btn1, javaLangStringMultiGameViewModelChoice1);
        }
        if ((dirtyFlags & 0x100L) != 0) {
            // api target 1

            this.btn1.setOnClickListener(mCallback5);
            this.btn2.setOnClickListener(mCallback6);
            this.btn3.setOnClickListener(mCallback7);
            this.btnHome.setOnClickListener(mCallback4);
            this.btnNext.setOnClickListener(mCallback8);
        }
        if ((dirtyFlags & 0x184L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btn2, javaLangStringMultiGameViewModelChoice2);
        }
        if ((dirtyFlags & 0x181L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btn3, javaLangStringMultiGameViewModelChoice3);
        }
        if ((dirtyFlags & 0x190L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView1, javaLangStringMultiGameViewModelNum1);
        }
        if ((dirtyFlags & 0x188L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView2, javaLangStringMultiGameViewModelNum2);
        }
        if ((dirtyFlags & 0x1a0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.txtAns, multiGameViewModelResultTextGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 5: {
                // localize variables for thread safety
                // multiGameViewModel
                com.example.plusgamefracment.MultiGameViewModel multiGameViewModel = mMultiGameViewModel;
                // multiGameViewModel != null
                boolean multiGameViewModelJavaLangObjectNull = false;



                multiGameViewModelJavaLangObjectNull = (multiGameViewModel) != (null);
                if (multiGameViewModelJavaLangObjectNull) {


                    multiGameViewModel.next();
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // multiGameViewModel
                com.example.plusgamefracment.MultiGameViewModel multiGameViewModel = mMultiGameViewModel;
                // multiGameViewModel != null
                boolean multiGameViewModelJavaLangObjectNull = false;



                multiGameViewModelJavaLangObjectNull = (multiGameViewModel) != (null);
                if (multiGameViewModelJavaLangObjectNull) {


                    multiGameViewModel.onSelectChoice2();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // multiGameViewModel
                com.example.plusgamefracment.MultiGameViewModel multiGameViewModel = mMultiGameViewModel;
                // multiGameViewModel != null
                boolean multiGameViewModelJavaLangObjectNull = false;



                multiGameViewModelJavaLangObjectNull = (multiGameViewModel) != (null);
                if (multiGameViewModelJavaLangObjectNull) {


                    multiGameViewModel.changeToHome();
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // multiGameViewModel
                com.example.plusgamefracment.MultiGameViewModel multiGameViewModel = mMultiGameViewModel;
                // multiGameViewModel != null
                boolean multiGameViewModelJavaLangObjectNull = false;



                multiGameViewModelJavaLangObjectNull = (multiGameViewModel) != (null);
                if (multiGameViewModelJavaLangObjectNull) {


                    multiGameViewModel.onSelectChoice3();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // multiGameViewModel
                com.example.plusgamefracment.MultiGameViewModel multiGameViewModel = mMultiGameViewModel;
                // multiGameViewModel != null
                boolean multiGameViewModelJavaLangObjectNull = false;



                multiGameViewModelJavaLangObjectNull = (multiGameViewModel) != (null);
                if (multiGameViewModelJavaLangObjectNull) {


                    multiGameViewModel.onSelectChoice1();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): multiGameViewModel.choice3
        flag 1 (0x2L): multiGameViewModel.choice1
        flag 2 (0x3L): multiGameViewModel.choice2
        flag 3 (0x4L): multiGameViewModel.num2
        flag 4 (0x5L): multiGameViewModel.num1
        flag 5 (0x6L): multiGameViewModel.resultText
        flag 6 (0x7L): multiGameViewModel.score
        flag 7 (0x8L): multiGameViewModel
        flag 8 (0x9L): null
    flag mapping end*/
    //end
}