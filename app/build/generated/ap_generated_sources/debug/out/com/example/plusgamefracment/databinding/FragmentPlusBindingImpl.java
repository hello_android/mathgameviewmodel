package com.example.plusgamefracment.databinding;
import com.example.plusgamefracment.R;
import com.example.plusgamefracment.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentPlusBindingImpl extends FragmentPlusBinding implements com.example.plusgamefracment.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.txtTitle, 11);
        sViewsWithIds.put(R.id.textView4, 12);
        sViewsWithIds.put(R.id.txtCorrect, 13);
        sViewsWithIds.put(R.id.txtWrong, 14);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback13;
    @Nullable
    private final android.view.View.OnClickListener mCallback11;
    @Nullable
    private final android.view.View.OnClickListener mCallback12;
    @Nullable
    private final android.view.View.OnClickListener mCallback10;
    @Nullable
    private final android.view.View.OnClickListener mCallback9;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentPlusBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private FragmentPlusBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 7
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[9]
            , (android.widget.Button) bindings[5]
            , (android.widget.Button) bindings[6]
            , (android.widget.Button) bindings[7]
            , (android.widget.Button) bindings[1]
            , (android.widget.Button) bindings[10]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[14]
            );
        this.amountCorrect.setTag(null);
        this.amountWrong.setTag(null);
        this.btn1.setTag(null);
        this.btn2.setTag(null);
        this.btn3.setTag(null);
        this.btnHome.setTag(null);
        this.btnNext.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.textView1.setTag(null);
        this.textView2.setTag(null);
        this.txtAns.setTag(null);
        setRootTag(root);
        // listeners
        mCallback13 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 5);
        mCallback11 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 3);
        mCallback12 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 4);
        mCallback10 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 2);
        mCallback9 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x100L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.plusGameViewModel == variableId) {
            setPlusGameViewModel((com.example.plusgamefracment.PlusGameViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setPlusGameViewModel(@Nullable com.example.plusgamefracment.PlusGameViewModel PlusGameViewModel) {
        this.mPlusGameViewModel = PlusGameViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x80L;
        }
        notifyPropertyChanged(BR.plusGameViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangePlusGameViewModelChoice1((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 1 :
                return onChangePlusGameViewModelNum2((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 2 :
                return onChangePlusGameViewModelNum1((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 3 :
                return onChangePlusGameViewModelResultText((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
            case 4 :
                return onChangePlusGameViewModelChoice3((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 5 :
                return onChangePlusGameViewModelChoice2((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 6 :
                return onChangePlusGameViewModelScore((androidx.lifecycle.LiveData<com.example.plusgamefracment.Score>) object, fieldId);
        }
        return false;
    }
    private boolean onChangePlusGameViewModelChoice1(androidx.lifecycle.LiveData<java.lang.Integer> PlusGameViewModelChoice1, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangePlusGameViewModelNum2(androidx.lifecycle.LiveData<java.lang.Integer> PlusGameViewModelNum2, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangePlusGameViewModelNum1(androidx.lifecycle.LiveData<java.lang.Integer> PlusGameViewModelNum1, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangePlusGameViewModelResultText(androidx.lifecycle.LiveData<java.lang.String> PlusGameViewModelResultText, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangePlusGameViewModelChoice3(androidx.lifecycle.LiveData<java.lang.Integer> PlusGameViewModelChoice3, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangePlusGameViewModelChoice2(androidx.lifecycle.LiveData<java.lang.Integer> PlusGameViewModelChoice2, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangePlusGameViewModelScore(androidx.lifecycle.LiveData<com.example.plusgamefracment.Score> PlusGameViewModelScore, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Integer plusGameViewModelNum1GetValue = null;
        java.lang.String javaLangStringPlusGameViewModelScoreCorrect = null;
        com.example.plusgamefracment.PlusGameViewModel plusGameViewModel = mPlusGameViewModel;
        java.lang.String javaLangStringPlusGameViewModelNum1 = null;
        androidx.lifecycle.LiveData<java.lang.Integer> plusGameViewModelChoice1 = null;
        java.lang.Integer plusGameViewModelChoice1GetValue = null;
        androidx.lifecycle.LiveData<java.lang.Integer> plusGameViewModelNum2 = null;
        androidx.lifecycle.LiveData<java.lang.Integer> plusGameViewModelNum1 = null;
        java.lang.Integer plusGameViewModelChoice3GetValue = null;
        java.lang.String javaLangStringPlusGameViewModelNum2 = null;
        java.lang.String javaLangStringPlusGameViewModelChoice3 = null;
        java.lang.String plusGameViewModelResultTextGetValue = null;
        com.example.plusgamefracment.Score plusGameViewModelScoreGetValue = null;
        androidx.lifecycle.LiveData<java.lang.String> plusGameViewModelResultText = null;
        java.lang.String javaLangStringPlusGameViewModelChoice2 = null;
        java.lang.String javaLangStringPlusGameViewModelScoreWrong = null;
        int plusGameViewModelScoreCorrect = 0;
        androidx.lifecycle.LiveData<java.lang.Integer> plusGameViewModelChoice3 = null;
        int plusGameViewModelScoreWrong = 0;
        java.lang.Integer plusGameViewModelNum2GetValue = null;
        androidx.lifecycle.LiveData<java.lang.Integer> plusGameViewModelChoice2 = null;
        java.lang.String javaLangStringPlusGameViewModelChoice1 = null;
        java.lang.Integer plusGameViewModelChoice2GetValue = null;
        androidx.lifecycle.LiveData<com.example.plusgamefracment.Score> plusGameViewModelScore = null;

        if ((dirtyFlags & 0x1ffL) != 0) {


            if ((dirtyFlags & 0x181L) != 0) {

                    if (plusGameViewModel != null) {
                        // read plusGameViewModel.choice1
                        plusGameViewModelChoice1 = plusGameViewModel.getChoice1();
                    }
                    updateLiveDataRegistration(0, plusGameViewModelChoice1);


                    if (plusGameViewModelChoice1 != null) {
                        // read plusGameViewModel.choice1.getValue()
                        plusGameViewModelChoice1GetValue = plusGameViewModelChoice1.getValue();
                    }


                    // read ("") + (plusGameViewModel.choice1.getValue())
                    javaLangStringPlusGameViewModelChoice1 = ("") + (plusGameViewModelChoice1GetValue);
            }
            if ((dirtyFlags & 0x182L) != 0) {

                    if (plusGameViewModel != null) {
                        // read plusGameViewModel.num2
                        plusGameViewModelNum2 = plusGameViewModel.getNum2();
                    }
                    updateLiveDataRegistration(1, plusGameViewModelNum2);


                    if (plusGameViewModelNum2 != null) {
                        // read plusGameViewModel.num2.getValue()
                        plusGameViewModelNum2GetValue = plusGameViewModelNum2.getValue();
                    }


                    // read ("") + (plusGameViewModel.num2.getValue())
                    javaLangStringPlusGameViewModelNum2 = ("") + (plusGameViewModelNum2GetValue);
            }
            if ((dirtyFlags & 0x184L) != 0) {

                    if (plusGameViewModel != null) {
                        // read plusGameViewModel.num1
                        plusGameViewModelNum1 = plusGameViewModel.getNum1();
                    }
                    updateLiveDataRegistration(2, plusGameViewModelNum1);


                    if (plusGameViewModelNum1 != null) {
                        // read plusGameViewModel.num1.getValue()
                        plusGameViewModelNum1GetValue = plusGameViewModelNum1.getValue();
                    }


                    // read ("") + (plusGameViewModel.num1.getValue())
                    javaLangStringPlusGameViewModelNum1 = ("") + (plusGameViewModelNum1GetValue);
            }
            if ((dirtyFlags & 0x188L) != 0) {

                    if (plusGameViewModel != null) {
                        // read plusGameViewModel.resultText
                        plusGameViewModelResultText = plusGameViewModel.getResultText();
                    }
                    updateLiveDataRegistration(3, plusGameViewModelResultText);


                    if (plusGameViewModelResultText != null) {
                        // read plusGameViewModel.resultText.getValue()
                        plusGameViewModelResultTextGetValue = plusGameViewModelResultText.getValue();
                    }
            }
            if ((dirtyFlags & 0x190L) != 0) {

                    if (plusGameViewModel != null) {
                        // read plusGameViewModel.choice3
                        plusGameViewModelChoice3 = plusGameViewModel.getChoice3();
                    }
                    updateLiveDataRegistration(4, plusGameViewModelChoice3);


                    if (plusGameViewModelChoice3 != null) {
                        // read plusGameViewModel.choice3.getValue()
                        plusGameViewModelChoice3GetValue = plusGameViewModelChoice3.getValue();
                    }


                    // read ("") + (plusGameViewModel.choice3.getValue())
                    javaLangStringPlusGameViewModelChoice3 = ("") + (plusGameViewModelChoice3GetValue);
            }
            if ((dirtyFlags & 0x1a0L) != 0) {

                    if (plusGameViewModel != null) {
                        // read plusGameViewModel.choice2
                        plusGameViewModelChoice2 = plusGameViewModel.getChoice2();
                    }
                    updateLiveDataRegistration(5, plusGameViewModelChoice2);


                    if (plusGameViewModelChoice2 != null) {
                        // read plusGameViewModel.choice2.getValue()
                        plusGameViewModelChoice2GetValue = plusGameViewModelChoice2.getValue();
                    }


                    // read ("") + (plusGameViewModel.choice2.getValue())
                    javaLangStringPlusGameViewModelChoice2 = ("") + (plusGameViewModelChoice2GetValue);
            }
            if ((dirtyFlags & 0x1c0L) != 0) {

                    if (plusGameViewModel != null) {
                        // read plusGameViewModel.score
                        plusGameViewModelScore = plusGameViewModel.getScore();
                    }
                    updateLiveDataRegistration(6, plusGameViewModelScore);


                    if (plusGameViewModelScore != null) {
                        // read plusGameViewModel.score.getValue()
                        plusGameViewModelScoreGetValue = plusGameViewModelScore.getValue();
                    }


                    if (plusGameViewModelScoreGetValue != null) {
                        // read plusGameViewModel.score.getValue().correct
                        plusGameViewModelScoreCorrect = plusGameViewModelScoreGetValue.getCorrect();
                        // read plusGameViewModel.score.getValue().wrong
                        plusGameViewModelScoreWrong = plusGameViewModelScoreGetValue.getWrong();
                    }


                    // read ("") + (plusGameViewModel.score.getValue().correct)
                    javaLangStringPlusGameViewModelScoreCorrect = ("") + (plusGameViewModelScoreCorrect);
                    // read ("") + (plusGameViewModel.score.getValue().wrong)
                    javaLangStringPlusGameViewModelScoreWrong = ("") + (plusGameViewModelScoreWrong);
            }
        }
        // batch finished
        if ((dirtyFlags & 0x1c0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.amountCorrect, javaLangStringPlusGameViewModelScoreCorrect);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.amountWrong, javaLangStringPlusGameViewModelScoreWrong);
        }
        if ((dirtyFlags & 0x181L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btn1, javaLangStringPlusGameViewModelChoice1);
        }
        if ((dirtyFlags & 0x100L) != 0) {
            // api target 1

            this.btn1.setOnClickListener(mCallback10);
            this.btn2.setOnClickListener(mCallback11);
            this.btn3.setOnClickListener(mCallback12);
            this.btnHome.setOnClickListener(mCallback9);
            this.btnNext.setOnClickListener(mCallback13);
        }
        if ((dirtyFlags & 0x1a0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btn2, javaLangStringPlusGameViewModelChoice2);
        }
        if ((dirtyFlags & 0x190L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btn3, javaLangStringPlusGameViewModelChoice3);
        }
        if ((dirtyFlags & 0x184L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView1, javaLangStringPlusGameViewModelNum1);
        }
        if ((dirtyFlags & 0x182L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView2, javaLangStringPlusGameViewModelNum2);
        }
        if ((dirtyFlags & 0x188L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.txtAns, plusGameViewModelResultTextGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 5: {
                // localize variables for thread safety
                // plusGameViewModel != null
                boolean plusGameViewModelJavaLangObjectNull = false;
                // plusGameViewModel
                com.example.plusgamefracment.PlusGameViewModel plusGameViewModel = mPlusGameViewModel;



                plusGameViewModelJavaLangObjectNull = (plusGameViewModel) != (null);
                if (plusGameViewModelJavaLangObjectNull) {


                    plusGameViewModel.next();
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // plusGameViewModel != null
                boolean plusGameViewModelJavaLangObjectNull = false;
                // plusGameViewModel
                com.example.plusgamefracment.PlusGameViewModel plusGameViewModel = mPlusGameViewModel;



                plusGameViewModelJavaLangObjectNull = (plusGameViewModel) != (null);
                if (plusGameViewModelJavaLangObjectNull) {


                    plusGameViewModel.onSelectChoice2();
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // plusGameViewModel != null
                boolean plusGameViewModelJavaLangObjectNull = false;
                // plusGameViewModel
                com.example.plusgamefracment.PlusGameViewModel plusGameViewModel = mPlusGameViewModel;



                plusGameViewModelJavaLangObjectNull = (plusGameViewModel) != (null);
                if (plusGameViewModelJavaLangObjectNull) {


                    plusGameViewModel.onSelectChoice3();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // plusGameViewModel != null
                boolean plusGameViewModelJavaLangObjectNull = false;
                // plusGameViewModel
                com.example.plusgamefracment.PlusGameViewModel plusGameViewModel = mPlusGameViewModel;



                plusGameViewModelJavaLangObjectNull = (plusGameViewModel) != (null);
                if (plusGameViewModelJavaLangObjectNull) {


                    plusGameViewModel.onSelectChoice1();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // plusGameViewModel != null
                boolean plusGameViewModelJavaLangObjectNull = false;
                // plusGameViewModel
                com.example.plusgamefracment.PlusGameViewModel plusGameViewModel = mPlusGameViewModel;



                plusGameViewModelJavaLangObjectNull = (plusGameViewModel) != (null);
                if (plusGameViewModelJavaLangObjectNull) {


                    plusGameViewModel.changeToHome();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): plusGameViewModel.choice1
        flag 1 (0x2L): plusGameViewModel.num2
        flag 2 (0x3L): plusGameViewModel.num1
        flag 3 (0x4L): plusGameViewModel.resultText
        flag 4 (0x5L): plusGameViewModel.choice3
        flag 5 (0x6L): plusGameViewModel.choice2
        flag 6 (0x7L): plusGameViewModel.score
        flag 7 (0x8L): plusGameViewModel
        flag 8 (0x9L): null
    flag mapping end*/
    //end
}