package com.example.plusgamefracment.databinding;
import com.example.plusgamefracment.R;
import com.example.plusgamefracment.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMinusBindingImpl extends FragmentMinusBinding implements com.example.plusgamefracment.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.txtTitle, 11);
        sViewsWithIds.put(R.id.textView4, 12);
        sViewsWithIds.put(R.id.txtCorrect, 13);
        sViewsWithIds.put(R.id.txtWrong, 14);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback17;
    @Nullable
    private final android.view.View.OnClickListener mCallback15;
    @Nullable
    private final android.view.View.OnClickListener mCallback16;
    @Nullable
    private final android.view.View.OnClickListener mCallback14;
    @Nullable
    private final android.view.View.OnClickListener mCallback18;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentMinusBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private FragmentMinusBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 7
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[8]
            , (android.widget.Button) bindings[4]
            , (android.widget.Button) bindings[5]
            , (android.widget.Button) bindings[6]
            , (android.widget.Button) bindings[10]
            , (android.widget.Button) bindings[9]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[14]
            );
        this.amountCorrect.setTag(null);
        this.amountWrong.setTag(null);
        this.btn1.setTag(null);
        this.btn2.setTag(null);
        this.btn3.setTag(null);
        this.btnHome.setTag(null);
        this.btnNext.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.textView1.setTag(null);
        this.textView2.setTag(null);
        this.txtAns.setTag(null);
        setRootTag(root);
        // listeners
        mCallback17 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 4);
        mCallback15 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 2);
        mCallback16 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 3);
        mCallback14 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 1);
        mCallback18 = new com.example.plusgamefracment.generated.callback.OnClickListener(this, 5);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x100L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.minusGameViewModel == variableId) {
            setMinusGameViewModel((com.example.plusgamefracment.MinusGameViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setMinusGameViewModel(@Nullable com.example.plusgamefracment.MinusGameViewModel MinusGameViewModel) {
        this.mMinusGameViewModel = MinusGameViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x80L;
        }
        notifyPropertyChanged(BR.minusGameViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeMinusGameViewModelNum1((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 1 :
                return onChangeMinusGameViewModelNum2((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 2 :
                return onChangeMinusGameViewModelChoice1((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 3 :
                return onChangeMinusGameViewModelChoice3((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 4 :
                return onChangeMinusGameViewModelChoice2((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 5 :
                return onChangeMinusGameViewModelScore((androidx.lifecycle.LiveData<com.example.plusgamefracment.Score>) object, fieldId);
            case 6 :
                return onChangeMinusGameViewModelResultText((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeMinusGameViewModelNum1(androidx.lifecycle.LiveData<java.lang.Integer> MinusGameViewModelNum1, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMinusGameViewModelNum2(androidx.lifecycle.LiveData<java.lang.Integer> MinusGameViewModelNum2, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMinusGameViewModelChoice1(androidx.lifecycle.LiveData<java.lang.Integer> MinusGameViewModelChoice1, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMinusGameViewModelChoice3(androidx.lifecycle.LiveData<java.lang.Integer> MinusGameViewModelChoice3, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMinusGameViewModelChoice2(androidx.lifecycle.LiveData<java.lang.Integer> MinusGameViewModelChoice2, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMinusGameViewModelScore(androidx.lifecycle.LiveData<com.example.plusgamefracment.Score> MinusGameViewModelScore, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMinusGameViewModelResultText(androidx.lifecycle.LiveData<java.lang.String> MinusGameViewModelResultText, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String javaLangStringMinusGameViewModelNum1 = null;
        java.lang.String javaLangStringMinusGameViewModelChoice1 = null;
        java.lang.Integer minusGameViewModelNum1GetValue = null;
        int minusGameViewModelScoreCorrect = 0;
        java.lang.String javaLangStringMinusGameViewModelScoreCorrect = null;
        java.lang.Integer minusGameViewModelChoice3GetValue = null;
        int minusGameViewModelScoreWrong = 0;
        java.lang.String javaLangStringMinusGameViewModelNum2 = null;
        androidx.lifecycle.LiveData<java.lang.Integer> minusGameViewModelNum1 = null;
        androidx.lifecycle.LiveData<java.lang.Integer> minusGameViewModelNum2 = null;
        com.example.plusgamefracment.MinusGameViewModel minusGameViewModel = mMinusGameViewModel;
        androidx.lifecycle.LiveData<java.lang.Integer> minusGameViewModelChoice1 = null;
        java.lang.String javaLangStringMinusGameViewModelChoice3 = null;
        androidx.lifecycle.LiveData<java.lang.Integer> minusGameViewModelChoice3 = null;
        java.lang.String minusGameViewModelResultTextGetValue = null;
        java.lang.Integer minusGameViewModelChoice1GetValue = null;
        androidx.lifecycle.LiveData<java.lang.Integer> minusGameViewModelChoice2 = null;
        com.example.plusgamefracment.Score minusGameViewModelScoreGetValue = null;
        java.lang.Integer minusGameViewModelNum2GetValue = null;
        java.lang.String javaLangStringMinusGameViewModelScoreWrong = null;
        java.lang.String javaLangStringMinusGameViewModelChoice2 = null;
        java.lang.Integer minusGameViewModelChoice2GetValue = null;
        androidx.lifecycle.LiveData<com.example.plusgamefracment.Score> minusGameViewModelScore = null;
        androidx.lifecycle.LiveData<java.lang.String> minusGameViewModelResultText = null;

        if ((dirtyFlags & 0x1ffL) != 0) {


            if ((dirtyFlags & 0x181L) != 0) {

                    if (minusGameViewModel != null) {
                        // read minusGameViewModel.num1
                        minusGameViewModelNum1 = minusGameViewModel.getNum1();
                    }
                    updateLiveDataRegistration(0, minusGameViewModelNum1);


                    if (minusGameViewModelNum1 != null) {
                        // read minusGameViewModel.num1.getValue()
                        minusGameViewModelNum1GetValue = minusGameViewModelNum1.getValue();
                    }


                    // read ("") + (minusGameViewModel.num1.getValue())
                    javaLangStringMinusGameViewModelNum1 = ("") + (minusGameViewModelNum1GetValue);
            }
            if ((dirtyFlags & 0x182L) != 0) {

                    if (minusGameViewModel != null) {
                        // read minusGameViewModel.num2
                        minusGameViewModelNum2 = minusGameViewModel.getNum2();
                    }
                    updateLiveDataRegistration(1, minusGameViewModelNum2);


                    if (minusGameViewModelNum2 != null) {
                        // read minusGameViewModel.num2.getValue()
                        minusGameViewModelNum2GetValue = minusGameViewModelNum2.getValue();
                    }


                    // read ("") + (minusGameViewModel.num2.getValue())
                    javaLangStringMinusGameViewModelNum2 = ("") + (minusGameViewModelNum2GetValue);
            }
            if ((dirtyFlags & 0x184L) != 0) {

                    if (minusGameViewModel != null) {
                        // read minusGameViewModel.choice1
                        minusGameViewModelChoice1 = minusGameViewModel.getChoice1();
                    }
                    updateLiveDataRegistration(2, minusGameViewModelChoice1);


                    if (minusGameViewModelChoice1 != null) {
                        // read minusGameViewModel.choice1.getValue()
                        minusGameViewModelChoice1GetValue = minusGameViewModelChoice1.getValue();
                    }


                    // read ("") + (minusGameViewModel.choice1.getValue())
                    javaLangStringMinusGameViewModelChoice1 = ("") + (minusGameViewModelChoice1GetValue);
            }
            if ((dirtyFlags & 0x188L) != 0) {

                    if (minusGameViewModel != null) {
                        // read minusGameViewModel.choice3
                        minusGameViewModelChoice3 = minusGameViewModel.getChoice3();
                    }
                    updateLiveDataRegistration(3, minusGameViewModelChoice3);


                    if (minusGameViewModelChoice3 != null) {
                        // read minusGameViewModel.choice3.getValue()
                        minusGameViewModelChoice3GetValue = minusGameViewModelChoice3.getValue();
                    }


                    // read ("") + (minusGameViewModel.choice3.getValue())
                    javaLangStringMinusGameViewModelChoice3 = ("") + (minusGameViewModelChoice3GetValue);
            }
            if ((dirtyFlags & 0x190L) != 0) {

                    if (minusGameViewModel != null) {
                        // read minusGameViewModel.choice2
                        minusGameViewModelChoice2 = minusGameViewModel.getChoice2();
                    }
                    updateLiveDataRegistration(4, minusGameViewModelChoice2);


                    if (minusGameViewModelChoice2 != null) {
                        // read minusGameViewModel.choice2.getValue()
                        minusGameViewModelChoice2GetValue = minusGameViewModelChoice2.getValue();
                    }


                    // read ("") + (minusGameViewModel.choice2.getValue())
                    javaLangStringMinusGameViewModelChoice2 = ("") + (minusGameViewModelChoice2GetValue);
            }
            if ((dirtyFlags & 0x1a0L) != 0) {

                    if (minusGameViewModel != null) {
                        // read minusGameViewModel.score
                        minusGameViewModelScore = minusGameViewModel.getScore();
                    }
                    updateLiveDataRegistration(5, minusGameViewModelScore);


                    if (minusGameViewModelScore != null) {
                        // read minusGameViewModel.score.getValue()
                        minusGameViewModelScoreGetValue = minusGameViewModelScore.getValue();
                    }


                    if (minusGameViewModelScoreGetValue != null) {
                        // read minusGameViewModel.score.getValue().correct
                        minusGameViewModelScoreCorrect = minusGameViewModelScoreGetValue.getCorrect();
                        // read minusGameViewModel.score.getValue().wrong
                        minusGameViewModelScoreWrong = minusGameViewModelScoreGetValue.getWrong();
                    }


                    // read ("") + (minusGameViewModel.score.getValue().correct)
                    javaLangStringMinusGameViewModelScoreCorrect = ("") + (minusGameViewModelScoreCorrect);
                    // read ("") + (minusGameViewModel.score.getValue().wrong)
                    javaLangStringMinusGameViewModelScoreWrong = ("") + (minusGameViewModelScoreWrong);
            }
            if ((dirtyFlags & 0x1c0L) != 0) {

                    if (minusGameViewModel != null) {
                        // read minusGameViewModel.resultText
                        minusGameViewModelResultText = minusGameViewModel.getResultText();
                    }
                    updateLiveDataRegistration(6, minusGameViewModelResultText);


                    if (minusGameViewModelResultText != null) {
                        // read minusGameViewModel.resultText.getValue()
                        minusGameViewModelResultTextGetValue = minusGameViewModelResultText.getValue();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x1a0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.amountCorrect, javaLangStringMinusGameViewModelScoreCorrect);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.amountWrong, javaLangStringMinusGameViewModelScoreWrong);
        }
        if ((dirtyFlags & 0x184L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btn1, javaLangStringMinusGameViewModelChoice1);
        }
        if ((dirtyFlags & 0x100L) != 0) {
            // api target 1

            this.btn1.setOnClickListener(mCallback14);
            this.btn2.setOnClickListener(mCallback15);
            this.btn3.setOnClickListener(mCallback16);
            this.btnHome.setOnClickListener(mCallback18);
            this.btnNext.setOnClickListener(mCallback17);
        }
        if ((dirtyFlags & 0x190L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btn2, javaLangStringMinusGameViewModelChoice2);
        }
        if ((dirtyFlags & 0x188L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btn3, javaLangStringMinusGameViewModelChoice3);
        }
        if ((dirtyFlags & 0x181L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView1, javaLangStringMinusGameViewModelNum1);
        }
        if ((dirtyFlags & 0x182L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView2, javaLangStringMinusGameViewModelNum2);
        }
        if ((dirtyFlags & 0x1c0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.txtAns, minusGameViewModelResultTextGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 4: {
                // localize variables for thread safety
                // minusGameViewModel != null
                boolean minusGameViewModelJavaLangObjectNull = false;
                // minusGameViewModel
                com.example.plusgamefracment.MinusGameViewModel minusGameViewModel = mMinusGameViewModel;



                minusGameViewModelJavaLangObjectNull = (minusGameViewModel) != (null);
                if (minusGameViewModelJavaLangObjectNull) {


                    minusGameViewModel.next();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // minusGameViewModel != null
                boolean minusGameViewModelJavaLangObjectNull = false;
                // minusGameViewModel
                com.example.plusgamefracment.MinusGameViewModel minusGameViewModel = mMinusGameViewModel;



                minusGameViewModelJavaLangObjectNull = (minusGameViewModel) != (null);
                if (minusGameViewModelJavaLangObjectNull) {


                    minusGameViewModel.onSelectChoice2();
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // minusGameViewModel != null
                boolean minusGameViewModelJavaLangObjectNull = false;
                // minusGameViewModel
                com.example.plusgamefracment.MinusGameViewModel minusGameViewModel = mMinusGameViewModel;



                minusGameViewModelJavaLangObjectNull = (minusGameViewModel) != (null);
                if (minusGameViewModelJavaLangObjectNull) {


                    minusGameViewModel.onSelectChoice3();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // minusGameViewModel != null
                boolean minusGameViewModelJavaLangObjectNull = false;
                // minusGameViewModel
                com.example.plusgamefracment.MinusGameViewModel minusGameViewModel = mMinusGameViewModel;



                minusGameViewModelJavaLangObjectNull = (minusGameViewModel) != (null);
                if (minusGameViewModelJavaLangObjectNull) {


                    minusGameViewModel.onSelectChoice1();
                }
                break;
            }
            case 5: {
                // localize variables for thread safety
                // minusGameViewModel != null
                boolean minusGameViewModelJavaLangObjectNull = false;
                // minusGameViewModel
                com.example.plusgamefracment.MinusGameViewModel minusGameViewModel = mMinusGameViewModel;



                minusGameViewModelJavaLangObjectNull = (minusGameViewModel) != (null);
                if (minusGameViewModelJavaLangObjectNull) {


                    minusGameViewModel.changeToHome();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): minusGameViewModel.num1
        flag 1 (0x2L): minusGameViewModel.num2
        flag 2 (0x3L): minusGameViewModel.choice1
        flag 3 (0x4L): minusGameViewModel.choice3
        flag 4 (0x5L): minusGameViewModel.choice2
        flag 5 (0x6L): minusGameViewModel.score
        flag 6 (0x7L): minusGameViewModel.resultText
        flag 7 (0x8L): minusGameViewModel
        flag 8 (0x9L): null
    flag mapping end*/
    //end
}