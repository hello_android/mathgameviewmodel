package com.example.plusgamefracment;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.example.plusgamefracment.databinding.ActivityMainBindingImpl;
import com.example.plusgamefracment.databinding.FragmentMinusBindingImpl;
import com.example.plusgamefracment.databinding.FragmentMultiBindingImpl;
import com.example.plusgamefracment.databinding.FragmentPlusBindingImpl;
import com.example.plusgamefracment.databinding.FragmentStartGameMenuBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYMAIN = 1;

  private static final int LAYOUT_FRAGMENTMINUS = 2;

  private static final int LAYOUT_FRAGMENTMULTI = 3;

  private static final int LAYOUT_FRAGMENTPLUS = 4;

  private static final int LAYOUT_FRAGMENTSTARTGAMEMENU = 5;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(5);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.plusgamefracment.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.plusgamefracment.R.layout.fragment_minus, LAYOUT_FRAGMENTMINUS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.plusgamefracment.R.layout.fragment_multi, LAYOUT_FRAGMENTMULTI);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.plusgamefracment.R.layout.fragment_plus, LAYOUT_FRAGMENTPLUS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.plusgamefracment.R.layout.fragment_start_game_menu, LAYOUT_FRAGMENTSTARTGAMEMENU);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYMAIN: {
          if ("layout/activity_main_0".equals(tag)) {
            return new ActivityMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTMINUS: {
          if ("layout/fragment_minus_0".equals(tag)) {
            return new FragmentMinusBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_minus is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTMULTI: {
          if ("layout/fragment_multi_0".equals(tag)) {
            return new FragmentMultiBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_multi is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTPLUS: {
          if ("layout/fragment_plus_0".equals(tag)) {
            return new FragmentPlusBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_plus is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSTARTGAMEMENU: {
          if ("layout/fragment_start_game_menu_0".equals(tag)) {
            return new FragmentStartGameMenuBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_start_game_menu is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(5);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "minusGameViewModel");
      sKeys.put(2, "multiGameViewModel");
      sKeys.put(3, "plusGameViewModel");
      sKeys.put(4, "startGamesMenuViewModel");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(5);

    static {
      sKeys.put("layout/activity_main_0", com.example.plusgamefracment.R.layout.activity_main);
      sKeys.put("layout/fragment_minus_0", com.example.plusgamefracment.R.layout.fragment_minus);
      sKeys.put("layout/fragment_multi_0", com.example.plusgamefracment.R.layout.fragment_multi);
      sKeys.put("layout/fragment_plus_0", com.example.plusgamefracment.R.layout.fragment_plus);
      sKeys.put("layout/fragment_start_game_menu_0", com.example.plusgamefracment.R.layout.fragment_start_game_menu);
    }
  }
}
