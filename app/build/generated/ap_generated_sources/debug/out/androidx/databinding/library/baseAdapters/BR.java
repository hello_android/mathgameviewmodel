package androidx.databinding.library.baseAdapters;

public class BR {
  public static final int _all = 0;

  public static final int minusGameViewModel = 1;

  public static final int multiGameViewModel = 2;

  public static final int plusGameViewModel = 3;

  public static final int startGamesMenuViewModel = 4;
}
